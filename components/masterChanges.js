import React from 'react';

import {StyleSheet, View, Text} from 'react-native';

const SayHello = () => {
  return (
    <View style={styles.container}>
      <Text>I have made changes to MASTER Branch</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SayHello;
